#!/bin/bash

targetfile="/usr/share/sounds/freedesktop/stereo/phone-incoming-call.oga" # works for mobian
uname -a | grep danctnix && targetfile="/usr/share/sounds/librem5/stereo/phone-incoming-call.oga"

function pick() {
zenity --question --title "Jinglebells" --text "Would you like to choose a new audio file to use for incoming call notifications?" || exit
[[ ! -e "$targetfile" ]] && zenity --error --title="Problem" --text "Destination $targetfile doesn't exist?" && exit 1
[[ ! -w "$targetfile" ]] && zenity --error --title="Problem" --text "Destination $targetfile not writable. Run sudo $0 install" && exit 1
newfile="$(zenity --file-selection)"
[[ ! -e $newfile ]] && zenity --error --title="Problem" --text "$newfile does not exist" && exit 1
ffmpeg -i "$newfile" -strict experimental -codec:v none -codec:a libvorbis -qscale:a 5 /tmp/jinglebells.oga -y & pidno=$!
while kill -s 0 $pidno; do sleep .1; done | (zenity --progress || zenity --info --title="Canceled" --text="Canceled" && kill $pidno)
[[ ! -e /tmp/jinglebells.oga ]] && zenity --error --title="Problem" --text "Failed to convert file" && exit 1
cp /tmp/jinglebells.oga "$targetfile"
#cp "$newfile" "$targetfile"
[[ $? -ne 0 ]] && zenity --error --title="Problem" --text "Failed to copy file" && exit 1
rm /tmp/jinglebells.oga
zenity --info --title="Success!" --text="Incoming call notification tone has been replaced with $newfile"
}

function install(){
set -ev
chgrp sudo "$targetfile" || chgrp wheel "$targetfile"
chmod 664 "$targetfile"
apk add ffmpeg zenity || pacman -Sy ffmpeg zenity || apt-get -y install ffmpeg zenity
#apk add zenity || pacman -Sy zenity || apt-get -y install zenity
cp $0 /usr/local/bin/jinglebells.sh
chmod +x /usr/local/bin/jinglebells.sh
(
cat <<'TEXT'
[Desktop Entry]
Name=Jinglebells
GenericName=Choose ringtone
Comment=Choose ringtone
Icon=preferences-system-notifications
Exec=/usr/local/bin/jinglebells.sh pick
Type=Application
StartupNotify=true
Terminal=false
TEXT
)>/usr/share/applications/jinglebells.desktop
echo
echo
echo "Jinglebells is ready for use."
}

case $1 in
	pick )
		pick ;;
	install )
		install ;;
	* )
		echo "Use: $0 [pick|install]"
		echo "Please run $0 install with sudo or as root"
esac
