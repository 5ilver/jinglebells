# jinglebells

jinglebells, ringtone smells, pinephone laid an ogg, pick a file to play while a new call rings for you

This script assumes that you are in the sudo  or wheel group. Install mode adds sudo  or wheel group write permissions to the default ringtone in phosh for mobian or arch, sets up needed packages, and adds a desktop shortcut in the app drawer.

In pick mode a file is selected to use as the new incoming call notification. It is converted with ffmpeg and then placed on top of the old ringtone file.

NOTE: ffmpeg conversion is working again. Tested with arch/phosh/mp3
